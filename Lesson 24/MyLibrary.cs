﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_24
{
    class MyLibrary
    {
        private Dictionary<string, Book> books = new Dictionary<string, Book>();
        //private Dictionary<string, Book> booksByAuthor = new Dictionary<string, Book>();
      

        public MyLibrary()
        {

        }

        public bool AddBook(Book book)
        {
            if (books.ContainsKey(book.Title))
            {
                return false;
            }
            else
            {
                books.Add(book.Title, book);
                return true;
            }
        }

        public bool RemoveBook(string title)
        {
            if (books.ContainsKey(title))
            {
                books.Remove(title);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool HaveThisBook(string title)
        {
            if (books.ContainsKey(title))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Book GetBook(string title)
        {
            if (books.ContainsKey(title))
            {
                return books[title];
            }
            else
            {
                return null;
            }
        }

        public Book GetBookByAuthor(string author)
        {
            foreach (var item in books)
            {
                if (item.Value.Author.Equals(author))   // "string1".Equals("string2") > correct. "string1" == "string2" > wrong
                {
                    return item.Value;
                }
            }
            return null;

            //if (booksByAuthor.ContainsKey(author))
            //{
            //    return books[author];
            //}
            //else
            //{
            //    return null;
            //}

        }

        public void Clear()
        {
            books.Clear();
        }

        public List<string> GetAuthors()
        {
            List<string> result = new List<string>();
            foreach (var item in books)
            {
                result.Add(item.Value.Author);
            }
            return result;
        }

        public List<Book> GetBooksSortedByAuthorName()
        {

            return books.Values.OrderBy(b => b.Author).ToList();
          
        }

        public List<string> GetBooksSortedByTitle()

        {
            return books.Values.OrderBy(b => b.Title).Select(b => b.Title).ToList();
            //books: Get Dictionary
            //1:book1(title,author)
            //2:book2(title,author)
            //3:book3(title,author)

            //1. Take Just Values
            //book1(title,author)
            //book2
            //book3(title,author)

            //2. Sorted List
            //book3(title,author)
            //book2(title,author)
            //book1(title,author)

            //3. Take Only Titles
            //moshe
            //yosii
            //rubi

            //4. Convert To List
            //Convert From Enumarble To List.
        }

        public int Count()
        {
            int count = 0;
           foreach (var item in books)
            {
                count++;
            }
            return count;
        }

        public override string ToString()
        {
            string result = "";

            foreach (var item in books)
            {
                result += item.Value.ToString() + " \n";
            }

            return result;

        }
    }
}

