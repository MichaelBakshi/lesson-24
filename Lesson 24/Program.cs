﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_24
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book1 = new Book("Anna Karenina", "Happy families are all alike; every unhappy family is unhappy in its own way", "A.N.Tolstoy", "Novel");
            Book book2 = new Book("Art of hearing heartbeats", "A love story set in Burma", "Philipp Sandker", "Novel");
            Book book3 = new Book("Cockroaches", "Harry is on a special mission", "Jo Nesbo", "Crime novel");

            //Dictionary<string, Book> library = new Dictionary<string, Book>()
            //{
            //    {book1.Title, book1 },
            //    {book2.Title,book2 },
            //    {book3.Title,book3 }
            //};

            MyLibrary myLibrary = new MyLibrary();
            myLibrary.AddBook(book1);
            myLibrary.AddBook(book2);
            myLibrary.AddBook(book3);

            

            Console.WriteLine(myLibrary.ToString());
            Console.WriteLine();

            Console.WriteLine(myLibrary.HaveThisBook("Anna Karenina"));
            Console.WriteLine();

            Console.WriteLine(myLibrary.GetBook("Anna Karenina"));
            Console.WriteLine();

            Console.WriteLine(myLibrary.GetBookByAuthor("Jo Nesbo"));
            Console.WriteLine();

            //Console.WriteLine(myLibrary.GetAuthors());
            foreach (var item in myLibrary.GetAuthors())
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            foreach (Book b in myLibrary.GetBooksSortedByAuthorName())
            {
                Console.WriteLine(b);
            }
            Console.WriteLine();

            foreach (var item in myLibrary.GetBooksSortedByTitle())
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            Console.WriteLine( myLibrary.Count());

        }
    }
}
